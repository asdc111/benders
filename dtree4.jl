###PARAMETER DEFINITION
basedir = "/Users/amulyayadav/Dropbox/Work/PostAAMAS/PhebeWork/Bottleneck/"
treeDepth = 2
timeLimit = 7200


####DATA PROCESSING. PREPARE TRAINING DATA
data = float(open(readdlm, basedir*"masterdataset-test-julia.csv"))
#data = data(:,2:size(data,2))
numFeatures = Int(size(data,2)-1)
numDataPoints = Int(size(data,1))

Xtrain = data[:,1:(size(data,2)-1)]
Ytrain = data[:,size(data,2)]

smallM = zeros(numDataPoints,1)

for i=1:numDataPoints
  min=10000.0
  for f=1:numFeatures
    diffVector = abs(Xtrain[i,f]*ones(numDataPoints,1) - Xtrain[:,f])
    minElement=10000.0
    for j=1:length(diffVector)
      if j==i
        continue
      elseif diffVector[j] < minElement
        minElement = diffVector[j]
      end
    end

    if minElement < min
      min = minElement
    end
  end
  smallM[i] = min
end

######MODEL SPECIFICATION
using JuMP,Gurobi

model = Model(solver=GurobiSolver())


numTreeNodeVars =Int(2^(treeDepth+1) - 1)
numLeafNodes = div(numTreeNodeVars, 2) + 1
numBranchNodes = div(numTreeNodeVars, 2)

@variable(model, 0 <= lin[1:numDataPoints, 1:numLeafNodes] <= 1)
@variable(model, 0<=x[1:numDataPoints,1:numLeafNodes]<=1)
@variable(model, 0<=z[1:numLeafNodes]<=1)
@variable(model, 0<=yplus[1:numDataPoints,1:numBranchNodes]<=1)
@variable(model, 0<=yminus[1:numDataPoints,1:numBranchNodes]<=1)
@variable(model, 0<=b[1:numBranchNodes]<=1)
@variable(model, 0<=tp[1:numDataPoints]<=1)
@variable(model, 0<=tn[1:numDataPoints]<=1)

@variable(model, 0<=a[1:numBranchNodes,1:numFeatures]<=1, Int)
@variable(model, 0<=w[1:numDataPoints,1:numBranchNodes]<=1, Int)


datasetOnes = Int64[]
datasetZeros = Int64[]
for i=1:numDataPoints
  if Ytrain[i]==1
    push!(datasetOnes,i)
  elseif Ytrain[i]==0
    push!(datasetZeros,i)
  end
end

@objective(model, Max, sum(tp[ datasetOnes[i] ] for i in 1:length(datasetOnes)) + sum(tn[ datasetZeros[i] ] for i in 1:length(datasetZeros)) )

for i=1:numDataPoints
  @constraint(model, sum(x[i,l] for l in 1:numLeafNodes) == 1)
end

for n=1:numBranchNodes
  @constraint(model, sum(a[n,f] for f in 1:numFeatures) == 1)

  rightNode = 2n+1
  rightIndices = Int64[]
  leftIndices = Int64[]


  for l=1:numLeafNodes
    currInd = l + numBranchNodes
    while currInd > rightNode
      currInd = div(currInd, 2)

      if currInd==rightNode || currInd==rightNode-1
        break
      end
    end

    if currInd == rightNode
      push!(rightIndices, l)
    elseif currInd == rightNode - 1
      push!(leftIndices, l)
    end
  end

  for i=1:numDataPoints
    @constraint(model, [l=1:length(rightIndices)], x[i, rightIndices[l]] <= 1 - w[i,n])
    @constraint(model, [l=1:length(leftIndices)], x[i, leftIndices[l]] <= w[i,n])
    @constraint(model, yplus[i,n] + yminus[i,n] >= smallM[i]*(1 - w[i,n]) )
    @constraint(model, b[n] - sum(a[n,f]*Xtrain[i,f] for f in 1:numFeatures) == yplus[i,n] - yminus[i,n])
    @constraint(model, w[i,n] >= yplus[i,n])
    @constraint(model, 1 - w[i,n] >= yminus[i,n])
  end
end

for i=1:numDataPoints
  for l=1:numLeafNodes
    @constraint(model, lin[i,l] >= z[l] + x[i,l] - 1)
    @constraint(model, lin[i,l] <= z[l] )
    @constraint(model, lin[i,l] <= x[i,l] )
  end

  if Ytrain[i] == 1
    @constraint(model, tp[i] == sum(lin[i,l] for l in 1:numLeafNodes))
  elseif Ytrain[i] == 0
    @constraint(model, tn[i] == sum(x[i,l] - lin[i,l] for l in 1:numLeafNodes))
  end
end

for n=1:numBranchNodes
  leftChild = 2n
  rightChild = 2n+1

  leftIndices = Int64[]
  rightIndices = Int64[]

  if (leftChild<=numBranchNodes)
    push!(leftIndices, leftChild)
  end
  if (rightChild<=numBranchNodes)
    push!(rightIndices, rightChild)
  end


  index=1
  while index <= length(leftIndices)
    element = leftIndices[index]
    if 2*element < numBranchNodes
      push!(leftIndices, 2*element)
    end
    if 2*element+1 < numBranchNodes
      push!(leftIndices, 2*element+1)
    end
    index = index+1
  end

  index=1
  while index <= length(rightIndices)
    element = rightIndices[index]
    if 2*element < numBranchNodes
      push!(rightIndices, 2*element)
    end
    if 2*element+1 < numBranchNodes
      push!(rightIndices, 2*element+1)
    end
    index = index + 1
  end

  for i=1:numDataPoints
    @constraint(model, [l = 1:length(rightIndices)], w[i, rightIndices[l]] <= 1 - w[i,n])
    @constraint(model, [l = 1:length(leftIndices)], w[i, leftIndices[l]] <= w[i,n])
  end
end



#THE MODEL IN A HUMAN-READABLE FORMAT
#------------------------------------
println("The optimization problem to be solved is:")
print(model) # Shows the model constructed in a human-readable form


####SET GUROBI PARAMS

###SOLVE PROBLEM
solve(model)
status
###PRINT OUTPUT
