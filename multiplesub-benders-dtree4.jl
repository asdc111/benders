#####BENDERS FOR A SINGLE MASTER AND MULTIPLE SUBPROBLEMS
##EXPECTS INPUT 
###PARAMETER DEFINITION
basedir = "/home/rcf-40/amulyaya/gurobi/MIO_Interpretable/datasets/1RndGame/"
treeDepth = 2
timeLimit = 7200


####DATA PROCESSING. PREPARE TRAINING DATA
data = float(open(readdlm, basedir*"masterdataset-mip-julia.csv"))
data = data[:,2:size(data,2)]
numFeatures = Int(size(data,2)-1)
numDataPoints = Int(size(data,1))

Xtrain = data[:,1:(size(data,2)-1)]
Ytrain = data[:,size(data,2)]

smallM = zeros(numDataPoints,1)

for i=1:numDataPoints
  min=10000.0
  for f=1:numFeatures
    diffVector = abs(Xtrain[i,f]*ones(numDataPoints,1) - Xtrain[:,f])
    minElement=10000.0
    for j=1:length(diffVector)
      if j==i
        continue
      elseif diffVector[j] < minElement
        minElement = diffVector[j]
      end
    end

    if minElement < min
      min = minElement
    end
  end
  smallM[i] = min
end

######MODEL SPECIFICATION
using JuMP,Gurobi

model = Model(solver=GurobiSolver(Presolve=0))


numTreeNodeVars =Int(2^(treeDepth+1) - 1)
numLeafNodes = div(numTreeNodeVars, 2) + 1
numBranchNodes = div(numTreeNodeVars, 2)

numAVars = numBranchNodes*numFeatures
numBVars = numBranchNodes
numWVars = numDataPoints*numBranchNodes
numZVars = numLeafNodes
numXVars = numDataPoints*numLeafNodes


#MASTER PROBLEM VARIABLES
@variable(model, 0<=a[1:numBranchNodes,1:numFeatures]<=1, Bin)
@variable(model, 0<=b[1:numBranchNodes]<=1)
@variable(model, 0<=w[1:numDataPoints,1:numBranchNodes]<=1, Bin)
@variable(model, 0<=z[1:numLeafNodes]<=1, Bin)

#SUB PROBLEM VARIABLES
@variable(model, 0<=x[1:numDataPoints,1:numLeafNodes]<=1)
@variable(model, 0<=lin[1:numDataPoints, 1:numLeafNodes] <= 1)
@variable(model, 0<=yplus[1:numDataPoints,1:numBranchNodes]<=1)
@variable(model, 0<=yminus[1:numDataPoints,1:numBranchNodes]<=1)
@variable(model, 0<=tp[1:numDataPoints]<=1)
@variable(model, 0<=tn[1:numDataPoints]<=1)


numSubProbVariables = 2numLeafNodes + 2numBranchNodes + 2




datasetOnes = Int64[]
datasetZeros = Int64[]
for i=1:numDataPoints
  if Ytrain[i]==1
    push!(datasetOnes,i)
  elseif Ytrain[i]==0
    push!(datasetZeros,i)
  end
end

@objective(model, Max, sum(tp[ datasetOnes[i] ] for i in 1:length(datasetOnes)) + sum(tn[ datasetZeros[i] ] for i in 1:length(datasetZeros)) )


numMasterConstraints=0
for n=1:numBranchNodes
  leftChild = 2n
  rightChild = 2n+1

  leftIndices = Int64[]
  rightIndices = Int64[]

  if (leftChild<=numBranchNodes)
    push!(leftIndices, leftChild)
  end
  if (rightChild<=numBranchNodes)
    push!(rightIndices, rightChild)
  end


  index=1
  while index <= length(leftIndices)
    element = leftIndices[index]
    if 2*element < numBranchNodes
      push!(leftIndices, 2*element)
    end
    if 2*element+1 < numBranchNodes
      push!(leftIndices, 2*element+1)
    end
    index = index+1
  end

  index=1
  while index <= length(rightIndices)
    element = rightIndices[index]
    if 2*element < numBranchNodes
      push!(rightIndices, 2*element)
    end
    if 2*element+1 < numBranchNodes
      push!(rightIndices, 2*element+1)
    end
    index = index + 1
  end

  for i=1:numDataPoints
    @constraint(model, [l = 1:length(rightIndices)], w[i, rightIndices[l]] + w[i,n] <= 1 )
    @constraint(model, [l = 1:length(leftIndices)], w[i, leftIndices[l]] -w[i,n] <= 0)
    numMasterConstraints = numMasterConstraints + length(rightIndices) + length(leftIndices)
  end

  @constraint(model, sum(a[n,f] for f in 1:numFeatures) <= 1)
  @constraint(model, -1*sum(a[n,f] for f in 1:numFeatures) <= -1)
  numMasterConstraints = numMasterConstraints + 2
end

##SUB PROBLEM CONSTRAINTS BY DATAPOINT
numSubProbConstraints=0
for i=1:numDataPoints
  @constraint(model, sum(x[i,l] for l in 1:numLeafNodes) <= 1)
  @constraint(model, -1*sum(x[i,l] for l in 1:numLeafNodes) <= -1)
  numSubProbConstraints=2

  for n=1:numBranchNodes
    rightNode = 2n+1
    rightIndices = Int64[]
    leftIndices = Int64[]

    for l=1:numLeafNodes
      currInd = l + numBranchNodes
      while currInd > rightNode
        currInd = div(currInd, 2)

        if currInd==rightNode || currInd==rightNode-1
          break
        end
      end

      if currInd == rightNode
        push!(rightIndices, l)
      elseif currInd == rightNode - 1
        push!(leftIndices, l)
      end
    end

    @constraint(model, [l=1:length(rightIndices)], x[i, rightIndices[l]]  + w[i,n] <= 1)
    @constraint(model, [l=1:length(leftIndices)], x[i, leftIndices[l]] - w[i,n] <= 0)
    @constraint(model, -yplus[i,n] - yminus[i,n] -smallM[i]* w[i,n] <= -smallM[i]  )
    @constraint(model, b[n] - sum(a[n,f]*Xtrain[i,f] for f in 1:numFeatures) -yplus[i,n] + yminus[i,n] <= 0)
    @constraint(model, -b[n] + sum(a[n,f]*Xtrain[i,f] for f in 1:numFeatures) + yplus[i,n] - yminus[i,n]<= 0)
    @constraint(model, yplus[i,n] - w[i,n] <=0)
    @constraint(model, yminus[i,n] +  w[i,n] <= 1)
    numSubProbConstraints = numSubProbConstraints + length(rightIndices) + length(leftIndices) + 5
  end

  for l=1:numLeafNodes
    @constraint(model, z[l] + x[i,l] - lin[i,l] <= 1)
    @constraint(model, lin[i,l] -z[l] <= 0 )
    @constraint(model, lin[i,l] -x[i,l]<= 0 )
    numSubProbConstraints = numSubProbConstraints + 3
  end

  if Ytrain[i] == 1
    @constraint(model, tp[i] - sum(lin[i,l] for l in 1:numLeafNodes) <=0 )
    @constraint(model, -tp[i] + sum(lin[i,l] for l in 1:numLeafNodes) <=0)
    numSubProbConstraints = numSubProbConstraints + 2
  elseif Ytrain[i] == 0
    @constraint(model, tn[i] - sum(x[i,l] - lin[i,l] for l in 1:numLeafNodes) <=0 )
    @constraint(model, -tn[i] + sum(x[i,l] - lin[i,l] for l in 1:numLeafNodes) <=0 )
    numSubProbConstraints = numSubProbConstraints + 2
  end
end


####MODEL ENDS. GET PROBLEM INTO STANDARD FORM AS ON SHUVOMOY'S PAGE
matrix = JuMP.prepConstrMatrix(model)
fullmat = full(matrix)

numColumnsinA1 = numAVars+numBVars+numWVars+numZVars
A = fullmat[1:numMasterConstraints,1:numColumnsinA1]

BTotal = JuMP.prepConstrBounds(model)[2]

B = BTotal[1:numMasterConstraints]

C = zeros(numAVars+numBVars+numWVars+numZVars)

Csub = zeros(numLeafNodes + numLeafNodes + numBranchNodes + numBranchNodes + 2, numDataPoints)
#c2 = [c2; Ytrain.*ones(numDataPoints); (ones(numDataPoints) - Ytrain).*ones(numDataPoints)]



Bsub = zeros(numSubProbConstraints, numDataPoints)

Amaster = zeros(numSubProbConstraints, numColumnsinA1, numDataPoints)

Asub = zeros(numSubProbConstraints, numSubProbVariables, numDataPoints)

currRowCounter = numMasterConstraints
for i=1:numDataPoints
  Csub[:,i] =[zeros(numLeafNodes + numLeafNodes + numBranchNodes + numBranchNodes); Ytrain[i]; (1-Ytrain[i])]
  Bsub[:,i] = BTotal[(currRowCounter+1):(currRowCounter+numSubProbConstraints)]
  Amaster[:,:,i] = fullmat[(currRowCounter+1):(currRowCounter+numSubProbConstraints), 1:numColumnsinA1]

  #Asub construction
  tempASub = zeros(numSubProbConstraints)
  #X's
  XstartingIndex = numColumnsinA1 + (i-1)*numLeafNodes + 1
  tempASub = hcat(tempASub, fullmat[(currRowCounter+1):(currRowCounter+numSubProbConstraints), XstartingIndex:(XstartingIndex + numLeafNodes - 1)])
  #lin's
  LinstartingIndex = numColumnsinA1 + numXVars + (i-1)*numLeafNodes + 1
  tempASub = hcat(tempASub, fullmat[(currRowCounter+1):(currRowCounter+numSubProbConstraints), LinstartingIndex:(LinstartingIndex + numLeafNodes - 1)])
  #yplus's
  YplusstartingIndex = numColumnsinA1 + numXVars + numXVars + (i-1)*numBranchNodes + 1
  tempASub = hcat(tempASub, fullmat[(currRowCounter+1):(currRowCounter+numSubProbConstraints), YplusstartingIndex:(YplusstartingIndex + numBranchNodes - 1)])
  #yminus's
  YminusstartingIndex = numColumnsinA1 + numXVars + numXVars + numWVars + (i-1)*numBranchNodes + 1
  tempASub = hcat(tempASub, fullmat[(currRowCounter+1):(currRowCounter+numSubProbConstraints), YminusstartingIndex:(YminusstartingIndex + numBranchNodes - 1)])
  #tp's
  tpstartingIndex = numColumnsinA1 + numXVars + numXVars + numWVars + numWVars + i
  tempASub = hcat(tempASub, fullmat[(currRowCounter+1):(currRowCounter+numSubProbConstraints), tpstartingIndex])
  #tp's
  tnstartingIndex = numColumnsinA1 + numXVars + numXVars + numWVars + numWVars + numDataPoints + i
  tempASub = hcat(tempASub, fullmat[(currRowCounter+1):(currRowCounter+numSubProbConstraints), tnstartingIndex])

  Asub[:,:,i] = tempASub[:,2:size(tempASub,2)]

  currRowCounter = currRowCounter + numSubProbConstraints
end

#BENDERS START
using JuMP, Gurobi

                    # Master Problem Description
                    # --------------------------

# Model name
tic()

masterProblemModel = Model(solver = GurobiSolver(Heuristics=0, Cuts = 0)) # If we want to add Benders lazy constraints
# in Gurobi, then we have to turn of Gurobi's own Cuts and Heuristics in the master problem

# Variable Definition (Only CplexSolver() works properly for these)
# ----------------------------------------------------------------
#@variable(masterProblemModel,  x[1:dimX] >=0 , Int)
#@variable(masterProblemModel, t)

# ***************ALTERNATIVE VARIABLE DEFINITION FOR GUROBI************
#If we replace the two lines above with the follwoing:
@variable(masterProblemModel,  0 <= x1[1:numAVars] <= 1 , Bin)
@variable(masterProblemModel,  0 <= x2[1:numBVars] <= 1)
@variable(masterProblemModel,  0 <= x3[1:numWVars] <= 1 , Bin)
@variable(masterProblemModel,  0 <= x4[1:numZVars] <= 1 , Bin)
@variable(masterProblemModel, 0 <= t[1:numDataPoints] <= 1e6)
# then all the solvers give the expected solution
#**********************************************************************
@constraint(masterProblemModel, [i=1:size(A,1)], sum(A[i,1:numAVars][j] * x1[j] for j in 1:numAVars) + sum(A[i,numAVars+1:numAVars+numBVars][j] * x2[j] for j in 1:numBVars) + sum(A[i,numAVars+numBVars+1:numAVars+numBVars+numWVars][j] * x3[j] for j in 1:numWVars) + sum(A[i,numAVars+numBVars+numWVars+1:numAVars+numBVars+numWVars+numZVars][j] * x4[j] for j in 1:numZVars) <= B[i])

# Objective Setting
# -----------------
@objective(masterProblemModel, Max, sum(t[i] for i in 1:numDataPoints) )

#print(masterProblemModel)

stringOfBenderCuts=String[] # this is an array of strings which will contain all the
# Benders cuts added to be displayed later

# There are no constraints when we start, so we will add all the constraints in the
# form of Benders cuts lazily

dimX = numAVars + numBVars + numWVars + numZVars
dimU = numSubProbConstraints

funcArray = []
for i=1:numDataPoints
  func = function (cb)
    #***************************************************************************
    # First we store the master problem solution in conventional data structures
    #println("----------------------------")
    println("ITERATION NUMBER = ", length(stringOfBenderCuts)+1)
    #println("---------------------------\n")

    fmCurrent = 0
    for j=1:numDataPoints
      fmCurrent = fmCurrent + getvalue(t[j])
    end
    xCurrent=Float64[]
    for j in 1:numAVars
        push!(xCurrent,getvalue(x1[j]))
    end
    for j in 1:numBVars
        push!(xCurrent,getvalue(x2[j]))
    end
    for j in 1:numWVars
        push!(xCurrent,getvalue(x3[j]))
    end
    for j in 1:numZVars
        push!(xCurrent,getvalue(x4[j]))
    end

    # Display the current solution of the master problem
    #println("MASTERPROBLEM INFORMATION")
    #println("-------------------------")
    #println("The master problem that was solved was:")
    #print(masterProblemModel)
    #println("with ", length(stringOfBenderCuts), " added lazy constraints")
    #println(stringOfBenderCuts)
    #println("Current Value of x is: ", xCurrent)
    #println("Current objective value of master problem, fmCurrent is: ", fmCurrent)
    #println("\n")

    #************************************************************************

    # ========================================================================
    #                         Now we solve the subproblem
    subProblemModel = Model(solver=GurobiSolver(Presolve=0, OutputFlag=0))

    dualCVal = Bsub[:,i] - Amaster[:,:,i]*xCurrent

    @variable(subProblemModel, u[1:dimU]>=0)

    @constraint(subProblemModel, [j=1:size(Asub[:,:,i],2)], sum(Asub[:,:,i][k,j]*u[k]  for k in 1:size(Asub[:,:,i],1))>=Csub[:,i][j])

    @objective(subProblemModel, Min, dot(C, xCurrent) + sum(dualCVal[j]*u[j] for j in 1:dimU))

    #println("The subproblem is being solved")

    statusSubProblem = solve(subProblemModel)

    # We store the results achieved from the subproblem in conventional data structures

    fsxCurrent = getobjectivevalue(subProblemModel)

    uCurrent = Float64[]
    for j in 1:dimU
        push!(uCurrent, getvalue(u[j]))
    end

    # Display the solution corresponding to the subproblem

    #println("SUBPROBLEM INFORMATION")
    #println("----------------------")
    #println("The subproblem that was solved was: ")
    #print(subProblemModel)
    #println("Current status of the subproblem is ", statusSubProblem)
    #println("Current Value of u is: ", uCurrent) # JuMP will return an extreme ray
    # automatically (if the solver supports it), so we do not need to change the syntax
    #println("Current Value of fs(xCurrent) is: ", fsxCurrent)
    #println("\n")

    # ==========================================================================



    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Now we check the status of the algorithm and add Benders cut when necessary
    dotVal = dot(Bsub[:,i], uCurrent)

    if statusSubProblem == :Optimal &&  fsxCurrent==fmCurrent # we are done
        println("OPTIMAL SOLUTION OF THE ORIGINAL PROBLEM FOUND :-)")
        println("The optimal objective value t is ", fmCurrent)
        println("The optimal x is ", xCurrent)
        #println("The optimal v is ", getDual(constrRefSubProblem))
        println("\n")
        return
    end

    println("-------------------ADDING LAZY CONSTRAINT----------------")
    if statusSubProblem == :Optimal && fsxCurrent < fmCurrent
        println("\nThere is a suboptimal vertex, add the corresponding constraint")
        cv= (Amaster[:,:,i])'*uCurrent - C
        @lazyconstraint(cb, t[i] + sum(cv[j]*x1[j] for j in 1:numAVars) + sum(cv[numAVars + j]*x2[j] for j in 1:numBVars) + sum(cv[numAVars + numBVars + j]*x3[j] for j in 1:numWVars) + sum(cv[numAVars + numBVars + numWVars + j]*x4[j] for j in 1:numZVars) <= dotVal )
        #println("t + ", cv, "ᵀ x <= ", dotVal)
        push!(stringOfBenderCuts, string("t+", cv, "'x <=", dotVal))
    end

    if statusSubProblem == :Unbounded
        println("\nThere is an  extreme ray, adding the corresponding constraint")
        ce = (Amaster[:,:,i])'*uCurrent
        @lazyconstraint(cb, sum(ce[j]*x1[j] for j in 1:numAVars) + sum(ce[numAVars + j]*x2[j] for j in 1:numBVars) + sum(ce[numAVars + numBVars + j]*x3[j] for j in 1:numWVars) + sum(ce[numAVars + numBVars + numWVars + j]*x4[j] for j in 1:numZVars) <= dotVal)
        #println(ce, "x <= ", dotVal)
        push!(stringOfBenderCuts, string(ce, "ᵀ x <= ", dotVal))
    end
    println("\n")
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  end
  push!(funcArray, func)
end

for i=1:numDataPoints
  addlazycallback(masterProblemModel, funcArray[i])
end
# Telling the solver to use the
# callback function

solve(masterProblemModel)
toc()

###PRINT OUTPUT
